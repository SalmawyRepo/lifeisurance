package com.company.loaninsurance.web.screens.insurancestatus;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.InsuranceStatus;

@UiController("InsuranceStatus.edit")
@UiDescriptor("insurance-status-edit.xml")
@EditedEntityContainer("insuranceStatusDc")
@LoadDataBeforeShow
public class InsuranceStatusEdit extends StandardEditor<InsuranceStatus> {
}
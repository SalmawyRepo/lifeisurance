package com.company.loaninsurance.web.screens.lifeinsurance;

import com.company.loaninsurance.entity.ClosingMonths;
import com.company.loaninsurance.web.screens.closingmonths.ClosingMonthsEdit;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataLoadContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.LifeInsurance;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@UiController("LifeInsurance.browse")
@UiDescriptor("life-insurance-browse.xml")
@LookupComponent("lifeInsurancesTable")
@LoadDataBeforeShow
public class LifeInsuranceBrowse extends StandardLookup<LifeInsurance> {

    @Inject
    private Screens screens;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionLoader<LifeInsurance> lifeInsurancesDl;
    @Inject
    private LookupField<ClosingMonths> closingMonthsLF;


    public void viewCalculateSceen(Component component) {

        ClosingMonthsEdit screen = screens.create(ClosingMonthsEdit.class);
        screen.setEntityToEdit( dataManager.create(ClosingMonths.class));
        screen.addAfterCloseListener(closeEvect->{

            lifeInsurancesDl.load();
        });
        screens.show(screen);






     //   screen.addAfterCloseListener();
  /*      screenBuilders.editor(customersTable)
                .withOpenMode(OpenMode.DIALOG)
                .withScreenClass(CustomerEdit.class)
                .withAfterCloseListener(afterScreenCloseEvent -> {
                    if (afterScreenCloseEvent.closedWith(StandardOutcome.COMMIT)) {
                        Customer committedCustomer = (afterScreenCloseEvent.getScreen()).getEditedEntity();
                        System.out.println("Updated " + committedCustomer);
                    }
                })
                .build()
                .show();*/




    }

    @Install(to = "lifeInsurancesDl", target = Target.DATA_LOADER)
    private List<LifeInsurance> lifeInsurancesDlLoadDelegate(LoadContext<LifeInsurance> loadContext) {
        LoadContext.Query query=loadContext.getQuery();
        Optional<ClosingMonths> closingMonth=Optional.ofNullable(closingMonthsLF.getValue());

        if(closingMonth.isPresent()){
        query.setQueryString("select e from LifeInsurance e where e.closingMonth.id= :closingMonthId")
                .setParameter("closingMonthId",closingMonth.get().getId());

        return dataManager.loadList(loadContext);}
        else
        return new ArrayList<>();
    }

    @Subscribe("closingMonthsLF")
    public void onClosingMonthsLFValueChange(HasValue.ValueChangeEvent<ClosingMonths> event) {

        lifeInsurancesDl.load();



    }






}
package com.company.loaninsurance.web.screens.lifeinsurance;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.LifeInsurance;

@UiController("LifeInsurance.edit")
@UiDescriptor("life-insurance-edit.xml")
@EditedEntityContainer("lifeInsuranceDc")
@LoadDataBeforeShow
public class LifeInsuranceEdit extends StandardEditor<LifeInsurance> {
}
package com.company.loaninsurance.web.screens.dangerplace;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.DangerPlace;

@UiController("DangerPlace.edit")
@UiDescriptor("danger-place-edit.xml")
@EditedEntityContainer("dangerPlaceDc")
@LoadDataBeforeShow
public class DangerPlaceEdit extends StandardEditor<DangerPlace> {
}
package com.company.loaninsurance.web.screens.insurancerequest;

import com.company.loaninsurance.entity.*;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;

import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.screen.*;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

@UiController("InsuranceRequest.edit")
@UiDescriptor("insurance-request-edit.xml")
@EditedEntityContainer("insuranceRequestDc")
@LoadDataBeforeShow
public class InsuranceRequestEdit extends StandardEditor<InsuranceRequest> {
    @Inject
    private DataManager dataManager;
    @Inject
    private Logger log;
    @Inject
    private CollectionLoader<VCustomerInfo> vCustomerInfoesDl;

    @Inject
    private LookupPickerField<VCustomerInfo> nationaIdLF;
    @Inject
    private LookupField<CustomerLoan> customerInfoField;

    @Inject
    private TextField<String> dangerMonthField;
    @Inject
    private TextField<String> customerAgeInDangerTimeField;
    @Inject
    private TextField<Double> NoMonthsBDandRequestField;

    @Inject
    private TextField<String> requestMonthField;
    @Inject
    private DateField<Date> dangerDateField;
    @Inject
    private DateField<Date> requestDateField;

    @Inject
    private Messages messages;
    @Inject
    private DataContext dataContext;
    @Inject
    private TextField<Double> NoMonthesBetweenD_andL_Field;

    @Inject
    private TextField<BigDecimal> diffCheckReqAmount;
    @Inject
    private TextField<BigDecimal> chequeAmountField;
    @Inject
    private TextField<BigDecimal> amountField;


    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
        if (PersistenceHelper.isNew(this.customerInfoField.getValue().getCustomer())) {
            log.info("customer is  new");
            dataContext.merge(this.customerInfoField.getValue().getCustomer());
        } else {
            log.info("customer is not  new");
        }

        getEditedEntity().setRequestStatus(InsuranceRequestStatusEnum.UNDER_REVISE);


        dataContext.merge(this.customerInfoField.getValue());
        log.info(dataContext.getModified().toString());
    }


    @Inject
    private CollectionLoader<VCustomerInfo> vCustomerInfoesDl2;


    @Subscribe
    public void onAfterShow(AfterShowEvent event) {

        if (!PersistenceHelper.isNew(this.getEditedEntity())) {

            log.info("is not new ");

            this.vCustomerInfoesDl2.load();


            CustomerLoan customerLoan = dataManager.load(CustomerLoan.class).view("customerLoan-full-view").one();
            customerInfoField.getValue().getCustomer();
            VCustomerInfo vcustomer = this.dataManager.load(VCustomerInfo.class).query("e.nationalId=?1", customerLoan.getCustomer().getId()).one();
            nationaIdLF.setValue(vcustomer);
            nationaIdLF.setValue(vcustomer);
            //=======================================================================
            this.calculateageOfCustAtDanger();
            this.calculateMonthsDangerAndRequest();


            this.calculateDangerMonth(dangerDateField.getValue());
            this.calculateRequestMonth(requestDateField.getValue());
            calculateMonthsDangerAndLoanStartDate();
            caculatediffCheckReqAmount();

        }

    }


    @Subscribe("nationaIdLF")
    public void onNationaIdLFValueChange(HasValue.ValueChangeEvent<VCustomerInfo> event) {
        vCustomerInfoesDl.setMaxResults(10);

        customerInfoField.setOptionsList(Arrays.asList(buildCustomerInfo(event.getValue())));
        customerInfoField.setValue(buildCustomerInfo(event.getValue()));


        //       customerInfoField.commit();
        vCustomerInfoesDl2.load();
        calculateageOfCustAtDanger();
        calculateMonthsDangerAndLoanStartDate();
    }


    @Subscribe("dangerDateField")
    public void onDangerDateFieldValueChange(HasValue.ValueChangeEvent<Date> event) {
        calculateDangerMonth(event.getValue());
        calculateMonthsDangerAndRequest();
        calculateageOfCustAtDanger();
        calculateMonthsDangerAndLoanStartDate();
    }

    private void calculateDangerMonth(Date dangerDate) {
        Locale locale = new Locale("ar");
        SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMM", locale);
        dangerMonthField.setValue(sdf.format(dangerDate));

    }

    @Subscribe("requestDateField")
    public void onRequestDateFieldValueChange(HasValue.ValueChangeEvent<Date> event) {
        calculateRequestMonth(event.getValue());
        calculateMonthsDangerAndRequest();
    }

    private void calculateRequestMonth(Date requestDate) {
        Locale locale = new Locale("ar");
        SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMM", locale);

        requestMonthField.setValue(sdf.format(requestDate));
    }

    private void calculateMonthsDangerAndRequest() {
        double value = 0.0;
        if (requestDateField.getValue() == null)
            log.info("request date is  null ");
        Optional<Date> requestDate = Optional.ofNullable(requestDateField.getValue());
        Optional<Date> dangerDate = Optional.ofNullable(dangerDateField.getValue());
        // log.info("type of data of danger Date ------------------+++++------ =>>> " + dangerDate.get().getClass().getName());
        if (requestDate.isPresent() && dangerDate.isPresent()) {

            LocalDate d1 = convertToLocalDateViaInstant(new Date(dangerDate.get().getTime()));
            LocalDate d2 = convertToLocalDateViaInstant(new Date(requestDate.get().getTime()));
            float months = subtractDates(d1, d2);
            value = months;
        }

        this.NoMonthsBDandRequestField.setValue(value);

    }


    //calculate number of monhtes between dangerDate and loan start date
    private void calculateMonthsDangerAndLoanStartDate() {
        double value = 0.0;
        Optional<Date> requestDate = Optional.ofNullable(requestDateField.getValue());
        Optional<VCustomerInfo> customInfo = Optional.ofNullable(nationaIdLF.getValue());

        if (requestDate.isPresent() && customInfo.isPresent()) {

            LocalDate d1 = convertToLocalDateViaInstant(new Date(customInfo.get().getDateAccOpen().getTime()));
            LocalDate d2 = convertToLocalDateViaInstant(new Date(requestDate.get().getTime()));
            float months = subtractDates(d1, d2);
            value = months;
        }

        this.NoMonthesBetweenD_andL_Field.setValue(value);

    }


    private float subtractDates(LocalDate date1, LocalDate date2) {


        Duration diff = Duration.between(date1.atStartOfDay(), date2.atStartOfDay());
        float diffMonths = diff.toDays() / 30;

        return diffMonths;

    }

    public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    private void calculateageOfCustAtDanger() {

        String value = "";
        Optional<VCustomerInfo> customInfo = Optional.ofNullable(nationaIdLF.getValue());
        Optional<Date> dangerDate = Optional.ofNullable(dangerDateField.getValue());
        log.info("dangerDate    " + dangerDate);
        if (customInfo.isPresent() && dangerDate.isPresent()) {


            LocalDate l1 = convertToLocalDateViaInstant(new Date(customInfo.get().getBirthDate().getTime()));
            LocalDate now1 = convertToLocalDateViaInstant(new Date(dangerDate.get().getTime()));
            Period age = Period.between(l1, now1);
            String yearLabel = messages.getMainMessage("label.year");
            this.customerAgeInDangerTimeField.setValue(yearLabel + " " + age.getYears());

            log.info("******************************* customer Age = " + age);

        }


    }


    public CustomerLoan buildCustomerInfo(VCustomerInfo customerInfo_OfView) {

        Customer customer = null;
        Optional<Customer> optionalCustomer = Optional.empty();
        try {

            optionalCustomer = Optional.ofNullable(dataManager.load(Customer.class).id(customerInfo_OfView.getNationalId()).one());


        } catch (NoResultException noResExp) {
            log.info("Customer not Registered before ");

        } catch (Exception e) {
            e.printStackTrace();

        }


        if (optionalCustomer.isPresent()) {
            customer = optionalCustomer.get();
        } else {
            customer = dataManager.create(Customer.class);

            customer.setBirthDate(customerInfo_OfView.getBirthDate());
            customer.setCodeCustomerId(customerInfo_OfView.getCodeCustomerId());
            customer.setCreditRating(customerInfo_OfView.getCreditRating());
            customer.setCustHomeBrn(customerInfo_OfView.getCustHomeBrn());
            customer.setCustHomeBrnName(customerInfo_OfView.getCustHomeBrnName());

            customer.setGendre(customerInfo_OfView.getGendre());
            customer.setId(customerInfo_OfView.getNationalId());
            customer.setNationalId(customerInfo_OfView.getNationalId());
            customer.setName(customerInfo_OfView.getName());
            customer.setProfession("TO DO");
        }
    /*
      ======================================================================================
      #                                                                                       #
       ======================================================================================
        */


        CustomerLoan customerLoan = null;


        Optional<CustomerLoan> optionalCustomerLoan = Optional.empty();
        try {
            optionalCustomerLoan = Optional.ofNullable(dataManager.load(CustomerLoan.class).id(customerInfo_OfView.getCodeAcctNo()).one());
        } catch (NoResultException noResExp) {
            log.info("no Customer loan Found ");
        } catch (Exception e) {
            e.printStackTrace();

        }

        if (optionalCustomerLoan.isPresent())
            customerLoan = optionalCustomerLoan.get();


        else {

            customerLoan = dataManager.create(CustomerLoan.class);
            customerLoan.setAcctOpenMonth(customerInfo_OfView.getAcctOpenMonth());
            customerLoan.setBalBook(customerInfo_OfView.getBalBook());
            customerLoan.setCodeRemmitterAcct(customerInfo_OfView.getCodeRemmitterAcct());
            customerLoan.setCodeAcctNo(customerInfo_OfView.getCodeAcctNo());
            customerLoan.setId(customerInfo_OfView.getCodeAcctNo());

            customerLoan.setCodProd(customerInfo_OfView.getCodProd());
            customerLoan.setDateAccOpen(customerInfo_OfView.getDateAccOpen());
            customerLoan.setNameProd(customerInfo_OfView.getNameProd());
            customerLoan.setDatOfMaturity(customerInfo_OfView.getDatOfMaturity());
            customerLoan.setCustomer(customer);

        }
        return customerLoan;


    }

    @Install(to = "vCustomerInfoesDl2", target = Target.DATA_LOADER)
    private List<VCustomerInfo> vCustomerInfoesDl2LoadDelegate(LoadContext<VCustomerInfo> loadContext) {
        LoadContext.Query query = loadContext.getQuery();
        query.setQueryString("select e from VCustomerInfo e where e.id= :id");
        String id = (nationaIdLF.getValue() == null) ? "0" : nationaIdLF.getValue().getId();
        query.setParameter("id", id);
        log.info(loadContext.getQuery().toString());
        return dataManager.loadList(loadContext);
    }

    public void disableScreen() {

        //   split.setEnabled(false);
        this.setReadOnly(true);
    }

    public void onRefuseBtnClick() {
        this.getEditedEntity().setRequestStatus(InsuranceRequestStatusEnum.REFUSED);

        dataManager.commit(this.getEditedEntity());

        dataManager.commit(this.getEditedEntity());

        this.close(new CloseAction() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        });
    }

    public void onConfirmBtnClick() {
        this.getEditedEntity().setRequestStatus(InsuranceRequestStatusEnum.CONFIRMED);
        dataManager.commit(this.getEditedEntity());


        this.close(new CloseAction() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        });

    }

    @Subscribe("amountField")
    public void onAmountFieldValueChange(HasValue.ValueChangeEvent<BigDecimal> event) {
        caculatediffCheckReqAmount();
    }

    @Subscribe("chequeAmountField")
    public void onChequeAmountFieldValueChange(HasValue.ValueChangeEvent<BigDecimal> event) {
        caculatediffCheckReqAmount();
    }


    private void caculatediffCheckReqAmount() {
        BigDecimal value = BigDecimal.ZERO;
        Optional<BigDecimal> chequeAmount = Optional.ofNullable(chequeAmountField.getValue());
        Optional<BigDecimal> requestAmount = Optional.ofNullable(amountField.getValue());

        if (chequeAmount.isPresent() && requestAmount.isPresent()) {


            value = requestAmount.get().subtract(chequeAmount.get());
        }

        this.diffCheckReqAmount.setValue(value);


    }
}
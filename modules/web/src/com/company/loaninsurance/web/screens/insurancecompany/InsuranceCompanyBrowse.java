package com.company.loaninsurance.web.screens.insurancecompany;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.InsuranceCompany;

@UiController("InsuranceCompany.browse")
@UiDescriptor("insurance-company-browse.xml")
@LookupComponent("insuranceCompaniesTable")
@LoadDataBeforeShow
public class InsuranceCompanyBrowse extends StandardLookup<InsuranceCompany> {
}
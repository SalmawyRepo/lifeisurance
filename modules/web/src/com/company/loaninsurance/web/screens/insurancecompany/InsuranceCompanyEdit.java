package com.company.loaninsurance.web.screens.insurancecompany;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.InsuranceCompany;

@UiController("InsuranceCompany.edit")
@UiDescriptor("insurance-company-edit.xml")
@EditedEntityContainer("insuranceCompanyDc")
@LoadDataBeforeShow
public class InsuranceCompanyEdit extends StandardEditor<InsuranceCompany> {
}
package com.company.loaninsurance.web.screens.dangerplace;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.DangerPlace;

@UiController("DangerPlace.browse")
@UiDescriptor("danger-place-browse.xml")
@LookupComponent("dangerPlacesTable")
@LoadDataBeforeShow
public class DangerPlaceBrowse extends StandardLookup<DangerPlace> {
}
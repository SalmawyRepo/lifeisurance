package com.company.loaninsurance.web.screens.closingmonths;

import com.company.loaninsurance.entity.LifeInsurance;
import com.company.loaninsurance.entity.VLifeInsurance;
import com.company.loaninsurance.service.MainService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.actions.list.ExcelAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.executors.BackgroundTask;
import com.haulmont.cuba.gui.executors.BackgroundWorker;
import com.haulmont.cuba.gui.executors.TaskLifeCycle;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.ClosingMonths;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@UiController("ClosingMonthes.edit")
@UiDescriptor("closing-months-edit.xml")
@EditedEntityContainer("closingMonthsDc")
@LoadDataBeforeShow
public class ClosingMonthsEdit extends StandardEditor<ClosingMonths> {

    @Inject
    private Dialogs dialogs;
    @Inject
    private GroupTable<LifeInsurance> lifeInsurancesTable;
    @Inject
    private TextField<BigDecimal> totalInsurancesInstallmentField;
    @Inject
    private Logger log;
    @Inject
    MainService service;
    @Inject
    private Messages messages;
    @Inject
    private DateField<Date> closingDateField;
    @Inject
    private TextField<BigDecimal> insurancePriceField;
    @Inject
    private Button calculateInsuranceInstallment;
    @Inject
    private DataContext dataContext;
    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionLoader<VLifeInsurance> lifeInsurances65Dl;

    @Subscribe("closingDateField")
    public void onClosingDateFieldValueChange(HasValue.ValueChangeEvent<Date> event) {
        Locale locale = new Locale("ar");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/  MMMMMMM", locale);
        String name=sdf.format(event.getValue());
        this.getEditedEntity().setName(name);
    }



    @Named("lifeInsurancesTable.excel")
    private ExcelAction lifeInsurancesTableExcel;
    @Inject
    private MainService mainService;

    @Inject
    private CollectionContainer<VLifeInsurance> lifeInsurancesDc;

    @Subscribe("lifeInsurancesTable.excel")
    public void onCustomersTableExcel(Action.ActionPerformedEvent event) {
        dialogs.createOptionDialog()
                .withCaption("Please confirm")
                .withMessage("Are you sure you want to print the content to XLS?")
                .withActions(
                        new DialogAction(DialogAction.Type.YES)
                                .withHandler(e -> lifeInsurancesTableExcel.execute()), // execute action
                        new DialogAction(DialogAction.Type.NO)
                )
                .show();
    }




    @Inject
    protected BackgroundWorker backgroundWorker;

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        disableOrEnableCalculate();

/*
     String closingMonthStatus=(PersistenceHelper.isNew(this.getEditedEntity())&& this.getEditedEntity().getClosingStatus().equals( ClosingMonthStatusEnum.CLOSED))?
             messages.getMainMessage("lifeInsurance.closingStatus.closed"):messages.getMainMessage("closingMonth.closingStatus.open");

*/


    }

    @Subscribe
    public void onInit(InitEvent event) {
        /*
        * lifeInsurance.operation.saved=تحت المراجعة
lifeInsurance.operation.validated=تمت المراجعة*/
    }

    public void onCalculateInsuranceInstallmentClick() {
    List<VLifeInsurance>list=    lifeInsurancesDc.getItems();

    List<VLifeInsurance>calculatedList=list.stream().map(e ->  mainService.calculateInsuranceInstallment(e,this.getEditedEntity())).collect(Collectors.toList());;
     //   lifeInsurancesDc.getItems().clear();
        BigDecimal toalinstalments=new BigDecimal(0);//BigDecimal.ZERO;

 
        for (VLifeInsurance e: calculatedList){
             toalinstalments= toalinstalments.add( e.getInsuranceInstlTotalCost());
}

         lifeInsurancesDc.getMutableItems().clear();
        lifeInsurancesDc.getMutableItems().addAll(calculatedList);
        totalInsurancesInstallmentField.setValue(toalinstalments);



    }








    @Subscribe("closingDateField")
    public void onClosingDateFieldValueChange1(HasValue.ValueChangeEvent<Date> event) {
        disableOrEnableCalculate();
    }

    @Subscribe("insurancePriceField")
    public void onInsurancePriceFieldValueChange(HasValue.ValueChangeEvent<BigDecimal> event) {
        disableOrEnableCalculate();
    }



    @Subscribe("insurancePriceField")
    public void onInsurancePriceFieldEnterPress(TextInputField.EnterPressEvent event) {
        if (disableOrEnableCalculate()){
            onCalculateInsuranceInstallmentClick();

        }
    }
    private boolean disableOrEnableCalculate(){
        Optional<Date> closingDate=Optional.ofNullable( this.closingDateField.getValue());
        Optional<BigDecimal> insurancePrice=Optional.ofNullable( this.insurancePriceField.getValue());
        boolean enable=(closingDate.isPresent()&&insurancePrice.isPresent());
        calculateInsuranceInstallment.setEnabled(enable);
log.info("this.insurancePriceField.getValue()=>>>"+this.insurancePriceField.getValue());
        log.info("  insurancePrice.isPresent() =>>>"+insurancePrice.isPresent());

        return enable;



    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {

List result=mainService.populateLifInsuranceNewEntities(this.lifeInsurancesDc.getMutableItems() , this.getEditedEntity());

       dataContext.merge(result);

    }

    @Install(to = "lifeInsurances65Dl", target = Target.DATA_LOADER)
    private List<VLifeInsurance> lifeInsurances65DlLoadDelegate(LoadContext<VLifeInsurance> loadContext) {

        Optional<Date>closingDateOptional=Optional.ofNullable(closingDateField.getValue());
        List<VLifeInsurance>  lifInsurancess=new ArrayList();
        if(closingDateOptional.isPresent()) {
          Date closingDate=   new Date(closingDateOptional.get().getTime());

            lifInsurancess = dataManager.loadList(loadContext).stream().
                    filter(e ->{


                        log.info("birthdate "+e.getBirthDate());
                        log.info("closingDate "+closingDate);
                        service.subtractDatesintoDays(e.getBirthDate(),e.getBirthDate());
                   return   (Math.abs( service.subtractDatesintoDays(closingDate,e.getBirthDate())) /365) >=65;
                   // return false;
                    })
                    .collect(Collectors.toList());

        }
            return lifInsurancess ;

    }

    @Subscribe("closingDateField")
    public void onClosingDateFieldValueChange2(HasValue.ValueChangeEvent<Date> event) {
        lifeInsurances65Dl.load();
    }


    private class LoadInsuranceLoans extends BackgroundTask {

        @Inject
        DataManager dataManager;

        protected LoadInsuranceLoans( Screen screen) {
            super(10, TimeUnit.MINUTES, screen);
        }

        @Override
        public Object run(TaskLifeCycle taskLifeCycle) throws Exception {


            List<VLifeInsurance> result= dataManager.load(VLifeInsurance.class).list();



            return null;
        }
    }














}

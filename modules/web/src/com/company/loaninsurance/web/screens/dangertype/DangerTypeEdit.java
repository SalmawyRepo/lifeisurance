package com.company.loaninsurance.web.screens.dangertype;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.DangerType;

@UiController("DangerType.edit")
@UiDescriptor("danger-type-edit.xml")
@EditedEntityContainer("dangerTypeDc")
@LoadDataBeforeShow
public class DangerTypeEdit extends StandardEditor<DangerType> {
}
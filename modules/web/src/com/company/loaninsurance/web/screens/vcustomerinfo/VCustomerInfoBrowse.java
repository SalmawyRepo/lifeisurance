package com.company.loaninsurance.web.screens.vcustomerinfo;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.VCustomerInfo;

@UiController("VCustomerInfo.browse")
@UiDescriptor("v-customer-info-browse.xml")
@LookupComponent("vCustomerInfoesTable")
@LoadDataBeforeShow
public class VCustomerInfoBrowse extends StandardLookup<VCustomerInfo> {
}
package com.company.loaninsurance.web.screens.customerinfo;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.CustomerInfo;

@UiController("CustomerInfo.browse")
@UiDescriptor("customer-info-browse.xml")
@LookupComponent("customerInfoesTable")
@LoadDataBeforeShow
public class CustomerInfoBrowse extends StandardLookup<CustomerInfo> {
}
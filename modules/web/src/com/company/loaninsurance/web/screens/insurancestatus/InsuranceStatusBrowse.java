package com.company.loaninsurance.web.screens.insurancestatus;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.InsuranceStatus;

@UiController("InsuranceStatus.browse")
@UiDescriptor("insurance-status-browse.xml")
@LookupComponent("insuranceStatusesTable")
@LoadDataBeforeShow
public class InsuranceStatusBrowse extends StandardLookup<InsuranceStatus> {
}
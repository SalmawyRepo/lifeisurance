package com.company.loaninsurance.web.screens.dangertype;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.DangerType;

@UiController("DangerType.browse")
@UiDescriptor("danger-type-browse.xml")
@LookupComponent("dangerTypesTable")
@LoadDataBeforeShow
public class DangerTypeBrowse extends StandardLookup<DangerType> {
}
package com.company.loaninsurance.web.screens.customer;

import com.company.loaninsurance.entity.ClosingMonths;
import com.company.loaninsurance.entity.InsuranceRequest;
import com.company.loaninsurance.web.screens.closingmonths.ClosingMonthsEdit;
import com.company.loaninsurance.web.screens.insurancerequest.InsuranceRequestBrowse;
import com.company.loaninsurance.web.screens.insurancerequest.InsuranceRequestEdit;
import com.company.loaninsurance.web.screens.lifeinsurance.LifeInsuranceBrowse;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.Customer;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.Optional;

@UiController("Customer.browse")
@UiDescriptor("customer-browse.xml")
@LookupComponent("customersTable")
@LoadDataBeforeShow
public class CustomerBrowse extends StandardLookup<Customer> {

    @Inject
    private Screens screens;
    @Inject
    DataManager dataManager;
    @Inject
    private GroupTable<Customer> customersTable;
    @Inject
    private CollectionLoader<Customer> customersDl;
    @Inject
    private Logger log;

    public void onAddnsuranceRequestClick() {


        InsuranceRequestEdit screen = screens.create(InsuranceRequestEdit.class);

        screen.setEntityToEdit( dataManager.create(InsuranceRequest.class));
        screen.addAfterCloseListener(closeEvect->{

            //   lifeInsurancesDl.load();
        });
        screens.show(screen);
    }

    public void onVewInsuranceRequestClick() {


        InsuranceRequestBrowse screen = screens.create(InsuranceRequestBrowse.class);
        Optional<Customer>customer=Optional.ofNullable(customersTable.getSingleSelected());
        screen.setCustomer(customer.get());

        screen.addAfterCloseListener(closeEvect->{

          customersDl.load();
        });
        screens.show(screen);
    }

    public void onSearchBtnClick() {
    }

    public void onCancelSearchBtnClick() {
    }

    @Subscribe
    public void onInit(InitEvent event) {
        customersTable.setItemClickAction( new BaseAction("tableClickAction"){

            @Override
            public void actionPerform(Component component) {
                onVewInsuranceRequestClick();
            }


        });
    }

 



}
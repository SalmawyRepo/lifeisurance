package com.company.loaninsurance.web.screens.insurancerequest;

import com.company.loaninsurance.entity.Customer;
import com.company.loaninsurance.entity.InsuranceRequestStatusEnum;
import com.company.loaninsurance.entity.UserGroupNameEnum;
import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.InsuranceRequest;
import com.haulmont.cuba.security.entity.UserRole;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.*;

@UiController("InsuranceRequest.browse")
@UiDescriptor("insurance-request-browse.xml")
@LookupComponent("insuranceRequestsTable")
@LoadDataBeforeShow
public class InsuranceRequestBrowse extends StandardLookup<InsuranceRequest> {
    @Inject
    private DataManager dataManager;
    @Inject
    private Screens screens;
    @Inject
    private GroupTable<InsuranceRequest> insuranceRequestsTable;
    @Inject
    private ScreenBuilders screenBuilders;
    @Inject
    private LookupField<Customer> customerNameValue;
    @Inject
    private LookupField<InsuranceRequestStatusEnum> StatusEnumLF_making;

    @Inject
    private Logger log;
    @Inject
    private CollectionLoader<InsuranceRequest> insuranceRequestsDl;
    @Inject
    private UserSession userSession;

    @Subscribe
    public void onInit(InitEvent event) {
        if(userSession.getUser().getGroup().getName().equals( UserGroupNameEnum.CHECKER.getId()) ){
            StatusEnumLF_making.setEditable(false);
        }



        StatusEnumLF_making.setValue(InsuranceRequestStatusEnum.UNDER_REVISE);
    }

    @Subscribe("StatusEnumLF_making")
    public void onStatusEnumLF_makingValueChange(HasValue.ValueChangeEvent<InsuranceRequestStatusEnum> event) {
        insuranceRequestsDl.load();
    }


    @Install(to = "insuranceRequestsDl", target = Target.DATA_LOADER)
    private List<InsuranceRequest> insuranceRequestsDlLoadDelegate(LoadContext<InsuranceRequest> loadContext) {
        insuranceRequestsDl.setMaxResults(10);
        Optional<InsuranceRequestStatusEnum> o = Optional.ofNullable(this.StatusEnumLF_making.getValue());
        Optional<Customer> customerOptional = Optional.ofNullable(this.customer);

        List data = new ArrayList<>();
        if (customerOptional.isPresent() && o.isPresent()) {
            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString("select e from InsuranceRequest e where e.customerLoan.customer.id= :customerId and e.requestStatus = :requestStatus")
                    .setParameter("customerId", customerOptional.get().getId())
                    .setParameter("requestStatus", o.get().getId());
            System.out.println(query.toString());
            data = dataManager.loadList(loadContext);
            System.out.println(data.size());


        }

        return data;
    }


    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {

        System.out.println(customer.getName());


        this.customer = customer;
    }


    public void onValidateBtnClick() {
    }

    public void onShowBtnClick() {


        Optional<InsuranceRequest> insuranceRequest = Optional.ofNullable(insuranceRequestsTable.getSingleSelected());
        InsuranceRequestEdit screen = screens.create(InsuranceRequestEdit.class);
        screen.setEntityToEdit(insuranceRequest.get());
        screen.disableScreen();

        screens.show(screen);
    }


    public void onRefuseBtnClick() {

        Set<InsuranceRequest> insuranceRequests = this.insuranceRequestsTable.getSelected();

        insuranceRequests.stream().forEach(e -> {

            e.setRequestStatus(InsuranceRequestStatusEnum.REFUSED);


        });
        CommitContext commitContext = new CommitContext(insuranceRequests);
        dataManager.commit(commitContext);
    }

    public void onConfirmBtnClick() {

        Set<InsuranceRequest> insuranceRequests = this.insuranceRequestsTable.getSelected();

        insuranceRequests.stream().forEach(e -> {
            e.setRequestStatus(InsuranceRequestStatusEnum.CONFIRMED);
        });


        CommitContext commitContext = new CommitContext(insuranceRequests);
        dataManager.commit(commitContext);
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        customerNameValue.setValue(customer);
        customerNameValue.setEditable(false);
        insuranceRequestsDl.load();

    }


    public void onSearchBtnClick() {

        //  log.info("search btn hass been clicked");
        insuranceRequestsDl.load();
        System.out.println("search btn hass been clicked");


    }

    public void onLoanStatusClick() {
       Optional<InsuranceRequest> requestOpt=Optional.ofNullable( this.insuranceRequestsTable.getSingleSelected());
      //  requestOpt.get().getCustomerLoan().


    }
}
package com.company.loaninsurance.web.screens.customerinfo;

import com.haulmont.cuba.gui.screen.*;
import com.company.loaninsurance.entity.CustomerInfo;

@UiController("CustomerInfo.edit")
@UiDescriptor("customer-info-edit.xml")
@EditedEntityContainer("customerInfoDc")
@LoadDataBeforeShow
public class CustomerInfoEdit extends StandardEditor<CustomerInfo> {
}
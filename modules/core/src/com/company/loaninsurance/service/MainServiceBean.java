package com.company.loaninsurance.service;

import com.company.loaninsurance.entity.ClosingMonths;
import com.company.loaninsurance.entity.LifeInsurance;
import com.company.loaninsurance.entity.VLifeInsurance;
import com.company.loaninsurance.entity.LifeInsurancetDocTypeEnum;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.Security;
import com.haulmont.cuba.core.global.UserSessionSource;
import org.slf4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service(MainService.NAME)
public class MainServiceBean implements MainService {



    @Inject
    private DataManager dataManager;


    @Inject
    private Persistence persistence;
    @Inject
    private Logger log;
    @Inject
    private Messages messages;
    @Inject
    private Security security;
    @Inject
    private ApplicationContext applicationContext;
    @Inject
    private UserSessionSource userSessionSource;
/*
    public LoadContext<CustomerInfo> getCustomerOFProduct700(){
        EntityManager em=persistence.getEntityManager();
        em.createQuery(" e.code_=700  ");

        LoadContext<CustomerInfo> loadContext=LoadContext.create(CustomerInfo.class);


}*/



    @Override
    public VLifeInsurance calculateInsuranceInstallment(VLifeInsurance lifeInsurance, ClosingMonths closingMonth){
        SimpleDateFormat monthYear=new SimpleDateFormat("yyyyMM");
        int loanDateInNumber=Integer.parseInt(monthYear.format(lifeInsurance.getLoanStartDate()));
        int closingMonthDateInNumber=Integer.parseInt(monthYear.format(closingMonth.getClosingDate()));

        if(closingMonthDateInNumber==loanDateInNumber)
        {





            lifeInsurance.setTypeId(LifeInsurancetDocTypeEnum.NEW.getId());
            lifeInsurance.setTypeName(  messages.getMainMessage("insuranceLife.type.name.new"));
            calculateNewInsuranceInstallment(lifeInsurance,closingMonth);

        }
        else

        {
          log.info("messages.getMainMessagePack() =>> "+messages.getMainMessagePack());

            lifeInsurance.setTypeId(LifeInsurancetDocTypeEnum.PRESENT.getId());
            lifeInsurance.setTypeName(  messages.getMainMessage("insuranceLife.type.name.present"));
             calculatePresentInsuranceInstallment(lifeInsurance,closingMonth);
        }
        return lifeInsurance;


    }
    private VLifeInsurance calculateNewInsuranceInstallment(VLifeInsurance lifeInsurance,ClosingMonths closingMonth){



        //get month length
        LocalDate startOfMonth=convertToLocalDateViaInstant(closingMonth.getClosingDate());
        YearMonth yearMonthObject = YearMonth.of(startOfMonth.getYear(), startOfMonth.getMonth());
        int daysOfMonth = yearMonthObject.lengthOfMonth(); //28

        //get number of days loan is open
        int daysLoanOpended= (int) (subtractDatesintoDays(closingMonth.getClosingDate(),lifeInsurance.getLoanStartDate())+1);

//calculate netInstallment
        BigDecimal netInstallment=BigDecimal.valueOf(daysLoanOpended/daysOfMonth)
                .multiply(closingMonth.getInsurancePrice())
                .multiply(lifeInsurance.getPrincipalBalance()
                        .divide(BigDecimal.valueOf(1000)));

        BigDecimal stampFee=BigDecimal.valueOf(0.75/100).multiply(netInstallment);
        BigDecimal documentHolderFee=BigDecimal.valueOf(0.2/100).multiply(netInstallment);
        BigDecimal reviewingFee=BigDecimal.valueOf(0.1/100).multiply(netInstallment);
        BigDecimal totalInstallment=netInstallment.add(stampFee).add(documentHolderFee).add(reviewingFee);

        lifeInsurance.setNoDaysLoanOpen(daysLoanOpended);
      //  lifeInsurance.setClosingMonth(closingMonth);
        lifeInsurance.setClosingMonthName(closingMonth.getName());

        lifeInsurance.setInsuranceInstlTotalCost(totalInstallment);
        lifeInsurance.setInsuranceNetInstallment(netInstallment);
        lifeInsurance.setReviewingFees(reviewingFee);
        lifeInsurance.setDocumentHolderCost(documentHolderFee);
        lifeInsurance.setStampCost(stampFee);

        return lifeInsurance;
    }
    @Override
    public float subtractDatesintoDays(LocalDate date1, LocalDate date2) {


        Duration diff = Duration.between(date1.atStartOfDay(), date2.atStartOfDay());
        return Math.abs(diff.toDays() );



    }
    @Override
    public float subtractDatesintoDays(Date date1, Date date2) {

        LocalDate d1=convertToLocalDateViaInstant(date1);
        LocalDate d2= convertToLocalDateViaInstant(date2);
        Duration diff = Duration.between(d1.atStartOfDay(), d2.atStartOfDay());
        return Math.abs(diff.toDays() );



    }
    @Override
    public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }



    private VLifeInsurance calculatePresentInsuranceInstallment(VLifeInsurance lifeInsurance,ClosingMonths closingMonth) {


        BigDecimal netInstallment = closingMonth.getInsurancePrice().multiply(lifeInsurance.getPrincipalBalance().divide(BigDecimal.valueOf(1000)));
        BigDecimal stampFee = BigDecimal.valueOf(0.75 / 100).multiply(netInstallment);
        BigDecimal documentHolderFee = BigDecimal.valueOf(0.2 / 100).multiply(netInstallment);
        BigDecimal reviewingFee = BigDecimal.valueOf(0.1 / 100).multiply(netInstallment);
        BigDecimal totalInstallment = netInstallment.add(stampFee).add(documentHolderFee).add(reviewingFee);

        lifeInsurance.setInsuranceInstlTotalCost(totalInstallment);
        lifeInsurance.setInsuranceNetInstallment(netInstallment);
        lifeInsurance.setReviewingFees(reviewingFee);
        lifeInsurance.setDocumentHolderCost(documentHolderFee);
        lifeInsurance.setStampCost(stampFee);
        lifeInsurance.setClosingMonthName(closingMonth.getName());
     //   lifeInsurance.setClosingMonth(closingMonth);
        return lifeInsurance;
    }








@Override
    public List<LifeInsurance>  populateLifInsuranceNewEntities(List<VLifeInsurance> vLifeInsuranceList, ClosingMonths closingMonths){


       return  vLifeInsuranceList.stream().map( vLifeInsurance -> {
         LifeInsurancetDocTypeEnum docTypeEnum=LifeInsurancetDocTypeEnum.fromId(vLifeInsurance.getTypeId());
log.info("vLifeInsurance.getTypeId()==>>>"+vLifeInsurance.getTypeId());
log.info("docTypeEnum =>..."+docTypeEnum);

            LifeInsurance lifeInsurance=   dataManager.create(LifeInsurance.class);
            lifeInsurance.setAccountId(vLifeInsurance.getAccountId ());
            lifeInsurance.setCustomerName(vLifeInsurance.getCustomerName ());
            lifeInsurance.setBranchCode(vLifeInsurance.getBranchCode ());
            lifeInsurance.setBranchName(vLifeInsurance.getBranchName ());
            lifeInsurance.setAccountNumber(vLifeInsurance.getAccountNumber ());
            lifeInsurance.setProductName(vLifeInsurance.getProductName ());
            lifeInsurance.setProductCode(vLifeInsurance.getProductCode ());
            lifeInsurance.setProfession(vLifeInsurance.getProfession ());
            lifeInsurance.setNationalId(vLifeInsurance.getNationalId ());
            lifeInsurance.setBirthDate(vLifeInsurance.getBirthDate ());
            lifeInsurance.setGendre(vLifeInsurance.getGendre ());
            lifeInsurance.setLoanStartDate(vLifeInsurance.getLoanStartDate ());
            lifeInsurance.setDateOfMaturity(vLifeInsurance.getDateOfMaturity ());
            lifeInsurance.setPrincipalBalance(vLifeInsurance.getPrincipalBalance ());
            lifeInsurance.setAmtDisbursed(vLifeInsurance.getAmtDisbursed ());
            lifeInsurance.setLoanPeriode(vLifeInsurance.getLoanPeriode ());





            lifeInsurance.setDocumentType(docTypeEnum);
            lifeInsurance.setClosingMonthName(vLifeInsurance.getClosingMonthName());
            lifeInsurance.setNoDaysLoanOpen(vLifeInsurance.getNoDaysLoanOpen ());
            lifeInsurance.setInsuranceNetInstallment(vLifeInsurance.getInsuranceNetInstallment ());
            lifeInsurance.setStampCost(vLifeInsurance.getStampCost ());
            lifeInsurance.setDocumentHolderCost(vLifeInsurance.getDocumentHolderCost ());
            lifeInsurance.setReviewingFees(vLifeInsurance.getReviewingFees ());
            lifeInsurance.setInsuranceInstlTotalCost(vLifeInsurance.getInsuranceInstlTotalCost());
            lifeInsurance.setClosingMonth(closingMonths);


            lifeInsurance.setLoanEndDate(vLifeInsurance.getLoanEndDate ());
            lifeInsurance.setNoUnpaid(vLifeInsurance.getNoUnpaid ());



            lifeInsurance.setS(vLifeInsurance.getS ());
            lifeInsurance.setR(vLifeInsurance.getR ());
            lifeInsurance.setAmtArrearsInterest(vLifeInsurance.getAmtArrearsInterest ());
            lifeInsurance.setAmtArrearsInterest(vLifeInsurance.getAmtArrearsInterest ());
            lifeInsurance.setAmtArrearsPrinc(vLifeInsurance.getAmtArrearsPrinc ());
            lifeInsurance.setAmtArrearsInterest(vLifeInsurance.getAmtArrearsInterest ());
            lifeInsurance.setCtrInstal(vLifeInsurance.getCtrInstal ());
            lifeInsurance.setL(vLifeInsurance.getL ());
            lifeInsurance.setIntrest(vLifeInsurance.getIntrest ());
            lifeInsurance.setInsuranceIntallment(vLifeInsurance.getInsuranceIntallment ());
            lifeInsurance.setTotal(vLifeInsurance.getTotal ());

log.info(lifeInsurance.toString());
            return lifeInsurance;
        }).collect(Collectors.toList());




//return null;

    }
}
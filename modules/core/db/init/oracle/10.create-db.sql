-- begin DANGER_PLACE
create table DANGER_PLACE (
    ID number(10),
    --
    NAME varchar2(255 char),
    CODE varchar2(255 char),
    --
    primary key (ID)
)^
-- end DANGER_PLACE
-- begin INSURANCE_REQUEST
create table INSURANCE_REQUEST (
    ID number(10),
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    VERSION number(10) not null,
    --
    DANGER_TYPE_ID number(10) not null,
    CHEQUE_AMOUNT number(19, 2),
    CUSTOMER_LOAN_ID varchar2(20 char) not null,
    DANGER_DATE date not null,
    AMOUNT number(19, 2) not null,
    DOC_RECEIVE_DATE date,
    REQUEST_DATE date,
    DANGER_CAUSE varchar2(255 char),
    DANGER_PLACE_ID number(10),
    INSURANCE_STATUS_ID number(10),
    REQUEST_STATUS varchar2(50 char),
    INSURANCE_STATUS_DATE date,
    INSURANCE_STATUS_DESC varchar2(255 char),
    CHECK_RECEIVE_DATE date,
    REFUSE_CAUSE varchar2(255 char),
    REFUSE_RECEIVE_DATE date,
    INSURANCE_COMPANY_ID number(10),
    --
    primary key (ID)
)^
-- end INSURANCE_REQUEST
-- begin DANGER_TYPE
create table DANGER_TYPE (
    ID number(10),
    --
    NAME varchar2(255 char),
    CODE varchar2(255 char),
    --
    primary key (ID)
)^
-- end DANGER_TYPE
-- begin CUSTOMER_INFO
create table CUSTOMER_INFO (
    ID number(10),
    --
    CODE_ varchar2(255 char),
    CREDIT_RATING char,
    NAM_CUST_FULL varchar2(1024 char),
    CUST_HOME_BRN number(10),
    CUST_HOME_BRN_NAME varchar2(1024 char),
    COD_ACCT_NO varchar2(100 char),
    COD_REMITTER_ACCT varchar2(100 char),
    COD_PROD number(10),
    NAM_PRODUCT varchar2(1024 char),
    COD_CUST_NATL_ID varchar2(50 char),
    DAT_BIRTH_CUST date,
    TXT_CUST_SEX varchar2(50 char),
    DAT_ACCT_OPEN date,
    DAT_OF_MATURITY date,
    ACCT_OPEN_MONTH number(10),
    BAL_BOOK float,
    TENOR_IN_MONTHS float,
    --
    primary key (ID)
)^
-- end CUSTOMER_INFO
-- begin INSURANCE_STATUS
create table INSURANCE_STATUS (
    ID number(10),
    --
    NAME varchar2(255 char),
    CODE varchar2(255 char),
    --
    primary key (ID)
)^
-- end INSURANCE_STATUS
-- begin INSURANCE_COMPANY
create table INSURANCE_COMPANY (
    ID number(10),
    --
    NAME varchar2(255 char),
    CODE varchar2(255 char),
    --
    primary key (ID)
)^
-- end INSURANCE_COMPANY
-- begin V_CUSTOMER_INFO
create table V_CUSTOMER_INFO (
    COD_ACCT_NO varchar2(100 char),
    --
    CREDIT_RATING char,
    ACCOUNT_STATUS varchar2(255 char),
    COD_CUST_ID varchar2(255 char),
    NAM_CUST_FULL varchar2(1024 char),
    CUST_HOME_BRN number(10),
    CUST_HOME_BRN_NAME varchar2(1024 char),
    COD_REMITTER_ACCT varchar2(100 char),
    COD_PROD number(10),
    NAM_PRODUCT varchar2(1024 char),
    COD_CUST_NATL_ID varchar2(50 char),
    DAT_BIRTH_CUST date,
    TXT_CUST_SEX varchar2(50 char),
    DAT_ACCT_OPEN date,
    DAT_OF_MATURITY date,
    ACCT_OPEN_MONTH varchar2(255 char),
    BAL_BOOK float,
    TENOR_IN_MONTHS float,
    --
    primary key (COD_ACCT_NO)
)^
-- end V_CUSTOMER_INFO
-- begin LIFE_INSURANCE
create table LIFE_INSURANCE (
    ID number(10),
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    VERSION number(10) not null,
    --
    COD_CC_BRN varchar2(15 char),
    CLOSING_MONTH_NAME varchar(255) null,
    CLOSING_MONTH_ID number(10) NULL,
    NO_DAYS_LOAN_OPEN number(10),
    INSURANCE_NET_INSTALLMENT number(15, 2),
    STAMP_COST number(19, 2),
    DOCUMENT_HOLDER_COST number(19, 2),
    REVIEWING_FEES number(19, 2),
    INSURANCE_INSTL_TOTAL_COST number(19, 2),
    COD_PROD varchar2(15 char),
    DOCUMENT_TYPE varchar2(50 char),
    NAM_BRANCH_SHRT varchar2(255 char),
    NAM_PRODUCT varchar2(255 char),
    COD_ACCT_NO varchar2(255 char),
    ACCOUNT_ID varchar2(255 char),
    COD_CUST_NATL_ID varchar2(255 char),
    DAT_BIRTH_CUST date,
    TXT_CUST_SEX char,
    NAM_CUST_FULL varchar2(255 char),
    CTR_INSTAL varchar2(255 char),
    CTR_TERM_MONTHS number(10),
    AMT_DISBURSED number(19, 2),
    PRINCIPAL_BALANCE number(19, 2),
    AMT_ARREARS_PRINC number(19, 2),
    AMT_ARREARS_INTEREST number(19, 2),
    TOTAL number(19, 2),
    KAST number(19, 2),
    FAIDA number(19, 2),
    NO_UNPAID number(10),
    DAT_OF_MATURITY date,
    L varchar2(255 char),
    S varchar2(255 char),
    R varchar2(255 char),
    TXT_PROFESSION varchar2(255 char),
    LOAN_START_DATE date,
    DAT_LAST_DISB date,
    --
    primary key (ID)
)^
-- end LIFE_INSURANCE
-- begin CLOSING_MONTHES
create table CLOSING_MONTHES (
    ID number(10),
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    VERSION number(10) not null,
    --
    CLOSING_DATE date,
    TOTAL_INSURANCES_INSTALLMENT number(19, 15),
    INSURANCE_PRICE number(19, 15),
    NAME varchar2(255 char),
    CLOSING_STATUS varchar2(50 char),
    STATUS integer,
    --
    primary key (ID)
)^
-- end CLOSING_MONTHES
-- begin CUSTOMER_LOANS
create table CUSTOMER_LOANS (
    COD_ACCT_NO varchar2(20 char),
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    VERSION number(10) not null,
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    --
    COD_REMITTER_ACCT varchar2(100 char),
    CUSTOMER_ID varchar2(20 char),
    COD_PROD number(10),
    NAM_PRODUCT varchar2(1024 char),
    DAT_ACCT_OPEN date,
    DAT_OF_MATURITY date,
    ACCT_OPEN_MONTH varchar2(255 char),
    BAL_BOOK float,
    --
    primary key (COD_ACCT_NO)
)^
-- end CUSTOMER_LOANS
-- begin CUSTOMERS
create table CUSTOMERS (
    NATIONAL_ID varchar2(20 char),
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    VERSION number(10) not null,
    --
    NAME varchar2(255 char) not null,
    PROFESSION varchar2(255 char),
    BIRTH_DATE date not null,
    CREDIT_RATING char,
    CUST_HOME_BRN number(10),
    CUST_HOME_BRN_NAME varchar2(1024 char),
    COD_CUST_ID varchar2(255 char),
    TXT_CUST_SEX varchar2(50 char),
    --
    primary key (NATIONAL_ID)
)^
-- end CUSTOMERS

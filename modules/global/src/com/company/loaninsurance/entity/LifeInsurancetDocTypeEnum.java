package com.company.loaninsurance.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum LifeInsurancetDocTypeEnum implements EnumClass<String> {

    NEW("new"),
    PRESENT("present");

    private String id;

    LifeInsurancetDocTypeEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static LifeInsurancetDocTypeEnum fromId(String id) {
        for (LifeInsurancetDocTypeEnum at : LifeInsurancetDocTypeEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
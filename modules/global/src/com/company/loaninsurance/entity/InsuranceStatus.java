package com.company.loaninsurance.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "INSURANCE_STATUS")
@Entity(name = "InsuranceStatus")
@NamePattern("%s|name")
public class InsuranceStatus extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 4876015977410883944L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
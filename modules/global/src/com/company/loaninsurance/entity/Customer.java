package com.company.loaninsurance.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseStringIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Versioned;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name = "CUSTOMERS")
@Entity(name = "Customer")
@NamePattern("%s|name")
public class Customer extends BaseStringIdEntity implements Creatable, Versioned, Updatable {
    private static final long serialVersionUID = 497684093968545010L;

    @Id
    @Column(name = "NATIONAL_ID", nullable = false, length = 20)
    private String id;

    @Column(name = "NATIONAL_ID", nullable = false, length = 20, insertable = false, updatable = false)
    private String nationalId;


    @Column(name = "NAME", nullable = false)
    @NotNull
    private String name;

    @Column(name = "PROFESSION")
    private String profession;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "BIRTH_DATE", nullable = false)
    private Date birthDate;

    @Column(name = "CREDIT_RATING")
    private Character creditRating;

    @Column(name = "CUST_HOME_BRN")
    private Integer custHomeBrn;

    @Column(name = "CUST_HOME_BRN_NAME", length = 1024)
    private String custHomeBrnName;

    @Column(name = "COD_CUST_ID")
    private String codeCustomerId;

    @Column(name = "TXT_CUST_SEX")
    private String gendre;

    @Column(name = "CREATE_TS")
    private Date createTs;

    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    @Column(name = "UPDATE_TS")
    private Date updateTs;

    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public void setGendre(GendreEnum gendre) {
        this.gendre = gendre == null ? null : gendre.getId();
    }

    public GendreEnum getGendre() {
        return gendre == null ? null : GendreEnum.fromId(gendre);
    }

    public Character getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(Character creditRating) {
        this.creditRating = creditRating;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public Integer getCustHomeBrn() {
        return custHomeBrn;
    }

    public void setCustHomeBrn(Integer custHomeBrn) {
        this.custHomeBrn = custHomeBrn;
    }

    public String getCustHomeBrnName() {
        return custHomeBrnName;
    }

    public void setCustHomeBrnName(String custHomeBrnName) {
        this.custHomeBrnName = custHomeBrnName;
    }

    public String getCodeCustomerId() {
        return codeCustomerId;
    }

    public void setCodeCustomerId(String codeCustomerId) {
        this.codeCustomerId = codeCustomerId;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }
}
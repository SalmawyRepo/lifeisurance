package com.company.loaninsurance.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum GendreEnum implements EnumClass<String> {

    FEMALE("F"),
    MALE("M");

    private String id;

    GendreEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static GendreEnum fromId(String id) {
        for (GendreEnum at : GendreEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
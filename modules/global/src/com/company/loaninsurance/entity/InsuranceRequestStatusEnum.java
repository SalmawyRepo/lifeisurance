package com.company.loaninsurance.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum InsuranceRequestStatusEnum implements EnumClass<String> {

    UNDER_REVISE("under_revision"),
    CONFIRMED("confirmed"),
    REFUSED("refused");


    private String id;

    InsuranceRequestStatusEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static InsuranceRequestStatusEnum fromId(String id) {
        for (InsuranceRequestStatusEnum at : InsuranceRequestStatusEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
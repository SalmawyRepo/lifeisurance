package com.company.loaninsurance.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum MakeCheckStatus implements EnumClass<Integer> {

    SAVED(1),
    VALIDATED(2);

    private Integer id;

    MakeCheckStatus(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static MakeCheckStatus fromId(Integer id) {
        for (MakeCheckStatus at : MakeCheckStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
package com.company.loaninsurance.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "DANGER_TYPE")
@Entity(name = "DangerType")
@NamePattern("%s|name")


public class DangerType extends BaseIntegerIdEntity {

    private static final long serialVersionUID = -948352376741379651L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
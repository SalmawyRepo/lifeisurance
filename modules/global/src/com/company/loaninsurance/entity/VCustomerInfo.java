package com.company.loaninsurance.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseStringIdEntity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "V_CUSTOMER_INFO")
@Entity(name = "VCustomerInfo")
@NamePattern("%s %s|name,nationalId")
public class VCustomerInfo extends BaseStringIdEntity {
    private static final long serialVersionUID = -465617870626111002L;

    @Column(name = "CREDIT_RATING")
    private Character creditRating;

    @Column(name = "ACCOUNT_STATUS")
    private String accountStatus;

    @Column(name = "COD_CUST_ID")
    private String codeCustomerId;

    @Column(name = "NAM_CUST_FULL", length = 1024)
    private String name;

    @Column(name = "CUST_HOME_BRN")
    private Integer custHomeBrn;

    @Column(name = "CUST_HOME_BRN_NAME", length = 1024)
    private String custHomeBrnName;


    @Column(name = "COD_ACCT_NO", insertable = false, updatable = false, nullable = false, length = 100)
    private String codeAcctNo;


    @Id
    @Column(name = "COD_ACCT_NO", nullable = false, length = 100)
    private String id;


    @Column(name = "COD_REMITTER_ACCT", length = 100)
    private String codeRemmitterAcct;

    @Column(name = "COD_PROD")
    private Integer codProd;

    @Column(name = "NAM_PRODUCT", length = 1024)
    private String nameProd;

    @Column(name = "COD_CUST_NATL_ID", length = 50)
    private String nationalId;

    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_BIRTH_CUST")
    private Date birthDate;

    @Column(name = "TXT_CUST_SEX")
    private String gendre;

    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_ACCT_OPEN")
    private Date dateAccOpen;

    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_OF_MATURITY")
    private Date datOfMaturity;

    @Column(name = "ACCT_OPEN_MONTH")
    private String acctOpenMonth;

    @Column(name = "BAL_BOOK")
    private Double balBook;

    @Column(name = "TENOR_IN_MONTHS")
    private Double tenorInMonths;

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }





    public void setGendre(GendreEnum gendre) {
        this.gendre = gendre == null ? null : gendre.getId();
    }

    public GendreEnum getGendre() {
        return gendre == null ? null : GendreEnum.fromId(gendre);
    }

    public void setAcctOpenMonth(String acctOpenMonth) {
        this.acctOpenMonth = acctOpenMonth;
    }

    public String getAcctOpenMonth() {
        return acctOpenMonth;
    }

    public String getCodeCustomerId() {
        return codeCustomerId;
    }

    public void setCodeCustomerId(String codeCustomerId) {
        this.codeCustomerId = codeCustomerId;
    }

    public void setCustHomeBrn(Integer custHomeBrn) {
        this.custHomeBrn = custHomeBrn;
    }

    public Integer getCustHomeBrn() {
        return custHomeBrn;
    }

    public Double getTenorInMonths() {
        return tenorInMonths;
    }

    public void setTenorInMonths(Double tenorInMonths) {
        this.tenorInMonths = tenorInMonths;
    }

    public Double getBalBook() {
        return balBook;
    }

    public void setBalBook(Double balBook) {
        this.balBook = balBook;
    }

    public Date getDatOfMaturity() {
        return datOfMaturity;
    }

    public void setDatOfMaturity(Date datOfMaturity) {
        this.datOfMaturity = datOfMaturity;
    }

    public Date getDateAccOpen() {
        return dateAccOpen;
    }

    public void setDateAccOpen(Date dateAccOpen) {
        this.dateAccOpen = dateAccOpen;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getNameProd() {
        return nameProd;
    }

    public void setNameProd(String nameProd) {
        this.nameProd = nameProd;
    }

    public Integer getCodProd() {
        return codProd;
    }

    public void setCodProd(Integer codProd) {
        this.codProd = codProd;
    }

    public String getCodeRemmitterAcct() {
        return codeRemmitterAcct;
    }

    public void setCodeRemmitterAcct(String codeRemmitterAcct) {
        this.codeRemmitterAcct = codeRemmitterAcct;
    }


    public String getCustHomeBrnName() {
        return custHomeBrnName;
    }

    public void setCustHomeBrnName(String custHomeBrnName) {
        this.custHomeBrnName = custHomeBrnName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(Character creditRating) {
        this.creditRating = creditRating;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id=id;
    }




    public String getCodeAcctNo() {
        return codeAcctNo;
    }

    public void setCodeAcctNo(String codeAcctNo) {
        this.codeAcctNo = codeAcctNo;
    }
}
package com.company.loaninsurance.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Versioned;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "CLOSING_MONTHES")
@Entity(name = "ClosingMonthes")
@NamePattern("%s|name")
public class ClosingMonths extends BaseIntegerIdEntity implements Creatable, Updatable, Versioned {
    private static final long serialVersionUID = 2150196996370819660L;

    @Temporal(TemporalType.DATE)
    @Column(name = "CLOSING_DATE")
    private Date closingDate;

    @Column(name = "TOTAL_INSURANCES_INSTALLMENT", precision = 19, scale = 15)
    private BigDecimal totalInsurancesInstallment;

    @Column(name = "INSURANCE_PRICE", precision = 19, scale = 15)
    private BigDecimal insurancePrice;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CLOSING_STATUS")
    private String closingStatus;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "CREATE_TS")
    private Date createTs;

    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    @Column(name = "UPDATE_TS")
    private Date updateTs;

    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public void setClosingStatus(ClosingMonthStatusEnum closingStatus) {
        this.closingStatus = closingStatus == null ? null : closingStatus.getId();
    }

    public ClosingMonthStatusEnum getClosingStatus() {
        return closingStatus == null ? null : ClosingMonthStatusEnum.fromId(closingStatus);
    }

    public void setStatus(MakeCheckStatus status) {
        this.status = status == null ? null : status.getId();
    }

    public MakeCheckStatus getStatus() {
        return status == null ? null : MakeCheckStatus.fromId(status);
    }


    public BigDecimal getInsurancePrice() {
        return insurancePrice;
    }

    public void setInsurancePrice(BigDecimal insurancePrice) {
        this.insurancePrice = insurancePrice;
    }

    public BigDecimal getTotalInsurancesInstallment() {
        return totalInsurancesInstallment;
    }

    public void setTotalInsurancesInstallment(BigDecimal totalInsurancesInstallment) {
        this.totalInsurancesInstallment = totalInsurancesInstallment;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }
}
package com.company.loaninsurance.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum ClosingMonthStatusEnum implements EnumClass<String> {

    CLOSED("closed"),
    OPEN("open");

    private String id;

    ClosingMonthStatusEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ClosingMonthStatusEnum fromId(String id) {
        for (ClosingMonthStatusEnum at : ClosingMonthStatusEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
package com.company.loaninsurance.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum UserGroupNameEnum implements EnumClass<String> {
MAKER("maker"),
    CHECKER("checker")
    ;

    private String id;

    UserGroupNameEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static UserGroupNameEnum fromId(String id) {
        for (UserGroupNameEnum at : UserGroupNameEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
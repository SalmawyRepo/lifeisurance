package com.company.loaninsurance.entity;

import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Versioned;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "INSURANCE_REQUEST")
@Entity(name = "InsuranceRequest")
public class InsuranceRequest extends BaseIntegerIdEntity implements Creatable, Versioned, Updatable {
    private static final long serialVersionUID = 4051731294086473077L;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "DANGER_TYPE_ID")
    @NotNull
    private DangerType dangerType;

    @Column(name = "CHEQUE_AMOUNT")
    private BigDecimal chequeAmount;

    @Lookup(type = LookupType.SCREEN, actions = "lookup")
    @OnDeleteInverse(DeletePolicy.DENY)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CUSTOMER_LOAN_ID")
    @NotNull
    private CustomerLoan customerLoan;

    @Temporal(TemporalType.DATE)
    @Column(name = "DANGER_DATE", nullable = false)
    @NotNull
    private Date dangerDate;

    @Column(name = "AMOUNT", nullable = false)
    @Digits(message = "{msg://msg.error.NumbersOnly}", integer = 9, fraction = 6)
    private BigDecimal amount;

    @Temporal(TemporalType.DATE)
    @Column(name = "DOC_RECEIVE_DATE")
    private Date docRecieveDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "REQUEST_DATE")
    private Date requestDate;

    @Column(name = "DANGER_CAUSE")
    private String dangerCause;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DANGER_PLACE_ID")
    private DangerPlace dangerPlace;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INSURANCE_STATUS_ID")
    private InsuranceStatus insuranceStatus;


    @Column(name = "REQUEST_STATUS")
    private String requestStatus;

    @Temporal(TemporalType.DATE)
    @Column(name = "INSURANCE_STATUS_DATE")
    private Date insuranceStatusDate;

    @Column(name = "INSURANCE_STATUS_DESC")
    private String insuranceStatusDesc;

    @Temporal(TemporalType.DATE)
    @Column(name = "CHECK_RECEIVE_DATE")
    private Date checkReceiveDate;

    @Column(name = "REFUSE_CAUSE")
    private String refuseCause;

    @Temporal(TemporalType.DATE)
    @Column(name = "REFUSE_RECEIVE_DATE")
    private Date refuseReceiveDate;


    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INSURANCE_COMPANY_ID")
    private InsuranceCompany insuranceCompany;

    @Column(name = "CREATE_TS")
    private Date createTs;

    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    @Column(name = "UPDATE_TS")
    private Date updateTs;

    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public InsuranceRequestStatusEnum getRequestStatus() {
        return requestStatus == null ? null : InsuranceRequestStatusEnum.fromId(requestStatus);
    }

    public void setRequestStatus(InsuranceRequestStatusEnum requestStatus) {
        this.requestStatus = requestStatus == null ? null : requestStatus.getId();
    }

    public void setCustomerLoan(CustomerLoan customerLoan) {
        this.customerLoan = customerLoan;
    }

    public CustomerLoan getCustomerLoan() {
        return customerLoan;
    }

    public BigDecimal getChequeAmount() {
        return chequeAmount;
    }

    public void setChequeAmount(BigDecimal chequeAmount) {
        this.chequeAmount = chequeAmount;
    }


    public Date getInsuranceStatusDate() {
        return insuranceStatusDate;
    }

    public void setInsuranceStatusDate(Date insuranceStatusDate) {
        this.insuranceStatusDate = insuranceStatusDate;
    }

    public String getInsuranceStatusDesc() {
        return insuranceStatusDesc;
    }

    public void setInsuranceStatusDesc(String insuranceStatusDesc) {
        this.insuranceStatusDesc = insuranceStatusDesc;
    }

    public DangerType getDangerType() {
        return dangerType;
    }

    public void setDangerType(DangerType dangerType) {
        this.dangerType = dangerType;
    }

    public Date getDangerDate() {
        return dangerDate;
    }

    public void setDangerDate(Date dangerDate) {
        this.dangerDate = dangerDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDocRecieveDate() {
        return docRecieveDate;
    }

    public void setDocRecieveDate(Date docRecieveDate) {
        this.docRecieveDate = docRecieveDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getDangerCause() {
        return dangerCause;
    }

    public void setDangerCause(String dangerCause) {
        this.dangerCause = dangerCause;
    }

    public DangerPlace getDangerPlace() {
        return dangerPlace;
    }

    public void setDangerPlace(DangerPlace dangerPlace) {
        this.dangerPlace = dangerPlace;
    }

    public InsuranceStatus getInsuranceStatus() {
        return insuranceStatus;
    }

    public void setInsuranceStatus(InsuranceStatus insuranceStatus) {
        this.insuranceStatus = insuranceStatus;
    }

    public Date getCheckReceiveDate() {
        return checkReceiveDate;
    }

    public void setCheckReceiveDate(Date checkReceiveDate) {
        this.checkReceiveDate = checkReceiveDate;
    }

    public String getRefuseCause() {
        return refuseCause;
    }

    public void setRefuseCause(String refuseCause) {
        this.refuseCause = refuseCause;
    }

    public Date getRefuseReceiveDate() {
        return refuseReceiveDate;
    }

    public void setRefuseReceiveDate(Date refuseReceiveDate) {
        this.refuseReceiveDate = refuseReceiveDate;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    @Override
    public String toString() {



        return super.toString();
    }
}
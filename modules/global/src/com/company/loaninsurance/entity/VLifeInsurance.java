package com.company.loaninsurance.entity;

import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.global.DbView;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@DbView
@Table(name = "V_LIFE_INSURANCE")
@Entity(name = "VLifeInsurance")
public class VLifeInsurance extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 8146663599320600510L;

    @Column(name = "COD_CC_BRN", length = 15)
    private String branchCode;

    @Column(name = "CLOSING_MONTH_NAME", columnDefinition = "varchar(255) null")
    private String closingMonthName;

    @Column(name = "NO_DAYS_LOAN_OPEN")
    private Integer noDaysLoanOpen;

    @Column(name = "INSURANCE_NET_INSTALLMENT", precision = 15, scale = 15)
    private BigDecimal insuranceNetInstallment;

    @Column(name = "STAMP_COST", precision = 19, scale = 15)
    private BigDecimal stampCost;

    @Column(name = "DOCUMENT_HOLDER_COST", precision = 19, scale = 15)
    private BigDecimal documentHolderCost;

    @Column(name = "REVIEWING_FEES", precision = 19, scale = 15)
    private BigDecimal reviewingFees;

    @Column(name = "INSURANCE_INSTL_TOTAL_COST", precision = 19, scale = 15)
    private BigDecimal insuranceInstlTotalCost;

    @Column(name = "COD_PROD", length = 15)
    private String productCode;

    @Column(name = "TYPE_NAME")
    private String typeName;

    @Column(name = "TYPE_ID")
    private String typeId;

    @Column(name = "NAM_BRANCH_SHRT")
    private String branchName;

    @Column(name = "NAM_PRODUCT")
    private String productName;

    @Column(name = "COD_ACCT_NO")
    private String accountNumber;

    @Column(name = "ACCOUNT_ID")
    private String accountId;

    @Column(name = "COD_CUST_NATL_ID")
    private String nationalId;

    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_BIRTH_CUST")
    private Date birthDate;

    @Column(name = "TXT_CUST_SEX")
    private Character gendre;

    @Column(name = "NAM_CUST_FULL")
    private String customerName;

    @Column(name = "CTR_INSTAL")
    private String ctrInstal;

    @Column(name = "CTR_TERM_MONTHS")
    private Integer loanPeriode;

    @Column(name = "AMT_DISBURSED")
    private BigDecimal amtDisbursed;

    @Column(name = "PRINCIPAL_BALANCE")
    private BigDecimal principalBalance;

    @Column(name = "AMT_ARREARS_PRINC")
    private BigDecimal amtArrearsPrinc;

    @Column(name = "AMT_ARREARS_INTEREST")
    private BigDecimal amtArrearsInterest;

    @Column(name = "TOTAL")
    private BigDecimal total;

    @Column(name = "KAST")
    private BigDecimal insuranceIntallment;

    @Column(name = "FAIDA")
    private BigDecimal intrest;

    @Column(name = "NO_UNPAID")
    private Integer noUnpaid;

    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_OF_MATURITY")
    private Date dateOfMaturity;

    @Column(name = "L")
    private String l;

    @Column(name = "S")
    private String s;

    @Column(name = "R")
    private String r;

    @Column(name = "TXT_PROFESSION")
    private String profession;

    @Temporal(TemporalType.DATE)
    @Column(name = "LOAN_START_DATE")
    private Date loanStartDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_LAST_DISB")
    private Date loanEndDate;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getClosingMonthName() {
        return closingMonthName;
    }

        public void setClosingMonthName(String closingMonthName) {
        this.closingMonthName = closingMonthName;
    }

        public String getTypeName() {
        return typeName;
    }

        public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

        public BigDecimal getInsuranceInstlTotalCost() {
        return insuranceInstlTotalCost;
    }

        public void setInsuranceInstlTotalCost(BigDecimal insuranceInstlTotalCost) {
        this.insuranceInstlTotalCost = insuranceInstlTotalCost;
    }

        public BigDecimal getReviewingFees() {
        return reviewingFees;
    }

        public void setReviewingFees(BigDecimal reviewingFees) {
        this.reviewingFees = reviewingFees;
    }

        public BigDecimal getDocumentHolderCost() {
        return documentHolderCost;
    }

        public void setDocumentHolderCost(BigDecimal documentHolderCost) {
        this.documentHolderCost = documentHolderCost;
    }

        public BigDecimal getStampCost() {
        return stampCost;
    }

        public void setStampCost(BigDecimal stampCost) {
        this.stampCost = stampCost;
    }

        public BigDecimal getInsuranceNetInstallment() {
        return insuranceNetInstallment;
    }

        public void setInsuranceNetInstallment(BigDecimal insuranceNetInstallment) {
        this.insuranceNetInstallment = insuranceNetInstallment;
    }

        public Integer getNoDaysLoanOpen() {
        return noDaysLoanOpen;
    }

        public void setNoDaysLoanOpen(Integer noDaysLoanOpen) {
        this.noDaysLoanOpen = noDaysLoanOpen;
    }

        public Integer getNoUnpaid() {
        return noUnpaid;
    }

        public void setNoUnpaid(Integer noUnpaid) {
        this.noUnpaid = noUnpaid;
    }

        public BigDecimal getIntrest() {
        return intrest;
    }

        public void setIntrest(BigDecimal intrest) {
        this.intrest = intrest;
    }

        public BigDecimal getInsuranceIntallment() {
        return insuranceIntallment;
    }

        public void setInsuranceIntallment(BigDecimal insuranceIntallment) {
        this.insuranceIntallment = insuranceIntallment;
    }

        public BigDecimal getTotal() {
        return total;
    }

        public void setTotal(BigDecimal total) {
        this.total = total;
    }

        public BigDecimal getAmtArrearsInterest() {
        return amtArrearsInterest;
    }

        public void setAmtArrearsInterest(BigDecimal amtArrearsInterest) {
        this.amtArrearsInterest = amtArrearsInterest;
    }

        public BigDecimal getAmtArrearsPrinc() {
        return amtArrearsPrinc;
    }

        public void setAmtArrearsPrinc(BigDecimal amtArrearsPrinc) {
        this.amtArrearsPrinc = amtArrearsPrinc;
    }

        public BigDecimal getPrincipalBalance() {
        return principalBalance;
    }

        public void setPrincipalBalance(BigDecimal principalBalance) {
        this.principalBalance = principalBalance;
    }

        public BigDecimal getAmtDisbursed() {
        return amtDisbursed;
    }

        public void setAmtDisbursed(BigDecimal amtDisbursed) {
        this.amtDisbursed = amtDisbursed;
    }

        public Integer getLoanPeriode() {
        return loanPeriode;
    }

        public void setLoanPeriode(Integer loanPeriode) {
        this.loanPeriode = loanPeriode;
    }

        public String getCtrInstal() {
        return ctrInstal;
    }

        public void setCtrInstal(String ctrInstal) {
        this.ctrInstal = ctrInstal;
    }

        public String getR() {
        return r;
    }

        public void setR(String r) {
        this.r = r;
    }

        public String getS() {
        return s;
    }

        public void setS(String s) {
        this.s = s;
    }

        public String getL() {
        return l;
    }

        public void setL(String l) {
        this.l = l;
    }

        public Date getLoanEndDate() {
        return loanEndDate;
    }

        public void setLoanEndDate(Date loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

        public Date getLoanStartDate() {
        return loanStartDate;
    }

        public void setLoanStartDate(Date loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

        public String getProfession() {
        return profession;
    }

        public void setProfession(String profession) {
        this.profession = profession;
    }

        public Date getDateOfMaturity() {
        return dateOfMaturity;
    }

        public void setDateOfMaturity(Date dateOfMaturity) {
        this.dateOfMaturity = dateOfMaturity;
    }

        public String getCustomerName() {
        return customerName;
    }

        public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

        public Character getGendre() {
        return gendre;
    }

        public void setGendre(Character gendre) {
        this.gendre = gendre;
    }

        public Date getBirthDate() {
        return birthDate;
    }

        public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

        public String getNationalId() {
        return nationalId;
    }

        public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

        public String getAccountId() {
        return accountId;
    }

        public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

        public String getAccountNumber() {
        return accountNumber;
    }

        public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

        public String getProductName() {
        return productName;
    }

        public void setProductName(String productName) {
        this.productName = productName;
    }

        public String getBranchName() {
        return branchName;
    }

        public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

        public String getProductCode() {
        return productCode;
    }

        public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

        public String getBranchCode() {
        return branchCode;
    }

        public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    }
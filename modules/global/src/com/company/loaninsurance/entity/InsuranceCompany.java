package com.company.loaninsurance.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "INSURANCE_COMPANY")
@Entity(name = "InsuranceCompany")
@NamePattern("%s|name")
public class InsuranceCompany extends BaseIntegerIdEntity {
    private static final long serialVersionUID = -5971083373077897726L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
package com.company.loaninsurance.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseStringIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Versioned;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import java.util.Date;

@Table(name = "CUSTOMER_LOANS")
@Entity(name = "CustomerLoan")
@NamePattern("%s|customer")
public class CustomerLoan extends BaseStringIdEntity implements Creatable, Versioned, Updatable {
    private static final long serialVersionUID = -181628629810887145L;

    @Id
    @Column(name = "COD_ACCT_NO", nullable = false, length = 20)
    private String id;


    @Column(name = "COD_ACCT_NO", insertable = false, updatable = false, nullable = false, length = 100)
    private String codeAcctNo;


    @Column(name = "COD_REMITTER_ACCT", length = 100)
    private String codeRemmitterAcct;

    @Lookup(type = LookupType.DROPDOWN, actions = {})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer customer;

    @Column(name = "COD_PROD")
    private Integer codProd;

    @Column(name = "NAM_PRODUCT", length = 1024)
    private String nameProd;


    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_ACCT_OPEN")
    private Date dateAccOpen;

    @Temporal(TemporalType.DATE)
    @Column(name = "DAT_OF_MATURITY")
    private Date datOfMaturity;

    @Column(name = "ACCT_OPEN_MONTH")
    private String acctOpenMonth;

    @Column(name = "BAL_BOOK")
    private Double balBook;

    @Column(name = "CREATE_TS")
    private Date createTs;

    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    @Column(name = "UPDATE_TS")
    private Date updateTs;

    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setAcctOpenMonth(String acctOpenMonth) {
        this.acctOpenMonth = acctOpenMonth;
    }

    public String getAcctOpenMonth() {
        return acctOpenMonth;
    }


    public Double getBalBook() {
        return balBook;
    }

    public void setBalBook(Double balBook) {
        this.balBook = balBook;
    }

    public Date getDatOfMaturity() {
        return datOfMaturity;
    }

    public void setDatOfMaturity(Date datOfMaturity) {
        this.datOfMaturity = datOfMaturity;
    }

    public Date getDateAccOpen() {
        return dateAccOpen;
    }

    public void setDateAccOpen(Date dateAccOpen) {
        this.dateAccOpen = dateAccOpen;
    }



    public String getNameProd() {
        return nameProd;
    }

    public void setNameProd(String nameProd) {
        this.nameProd = nameProd;
    }

    public Integer getCodProd() {
        return codProd;
    }

    public void setCodProd(Integer codProd) {
        this.codProd = codProd;
    }

    public String getCodeRemmitterAcct() {
        return codeRemmitterAcct;
    }

    public void setCodeRemmitterAcct(String codeRemmitterAcct) {
        this.codeRemmitterAcct = codeRemmitterAcct;
    }


    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id=id;
    }


    public String getCodeAcctNo() {
        return codeAcctNo;
    }

    public void setCodeAcctNo(String codeAcctNo) {
        this.codeAcctNo = codeAcctNo;
    }










}
package com.company.loaninsurance.service;

import com.company.loaninsurance.entity.ClosingMonths;
import com.company.loaninsurance.entity.LifeInsurance;
import com.company.loaninsurance.entity.VLifeInsurance;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface MainService {
    String NAME = "MainService";

    VLifeInsurance calculateInsuranceInstallment(VLifeInsurance lifeInsurance, ClosingMonths closingMonth);

    float subtractDatesintoDays(LocalDate date1, LocalDate date2);

    float subtractDatesintoDays(Date date1, Date date2);

    LocalDate convertToLocalDateViaInstant(Date dateToConvert);

    List<LifeInsurance> populateLifInsuranceNewEntities(List<VLifeInsurance> vLifeInsuranceList, ClosingMonths closingMonths);
}